pdm-pep517 (1.1.4+ds1-2) unstable; urgency=medium

  * QA upload.
  * Orphan the package.

 -- Boyuan Yang <byang@debian.org>  Thu, 29 Feb 2024 14:04:16 -0500

pdm-pep517 (1.1.4+ds1-1) unstable; urgency=medium

  * new upstream legacy release.

 -- Boyuan Yang <byang@debian.org>  Tue, 13 Jun 2023 11:58:30 -0400

pdm-pep517 (1.1.3+ds1-1~exp1) experimental; urgency=medium

  * New upstream legacy release.

 -- Boyuan Yang <byang@debian.org>  Tue, 04 Apr 2023 15:45:11 -0400

pdm-pep517 (1.1.2+ds1-1) unstable; urgency=medium

  * New upstream legacy release.

 -- Boyuan Yang <byang@debian.org>  Tue, 07 Feb 2023 22:50:02 -0500

pdm-pep517 (1.1.1+ds1-1) unstable; urgency=medium

  * New upstream legacy release.
  * debian/patches: Update and drop backported patches.
  * debian/watch: Monitor legacy 1.x branch.
  * debian/control:
    + Update homepage URL.
    + Tighten version requirement of python3-packaging (23+).

 -- Boyuan Yang <byang@debian.org>  Tue, 31 Jan 2023 12:01:14 -0500

pdm-pep517 (1.0.6+ds1-2) unstable; urgency=high

  * debian/patches/0002-scm.py-Patch-for-python3-packaging-22.patch:
    Cherry-pick upstream fix on compatibility with new python3-packaging.
    (Closes: #1027496)

 -- Boyuan Yang <byang@debian.org>  Tue, 03 Jan 2023 14:30:23 -0500

pdm-pep517 (1.0.6+ds1-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Sun, 27 Nov 2022 14:34:52 -0500

pdm-pep517 (1.0.5+ds1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches: Refresh patches.
  * debian/watch: Use dversionmangle=auto (lintian).

 -- Boyuan Yang <byang@debian.org>  Tue, 25 Oct 2022 17:00:27 -0400

pdm-pep517 (1.0.4+ds1-1) unstable; urgency=medium

  * New upstream release.
  * debian/README.Debian: Add hints for pdm-pep517 backend users.
  * debian/patches: Refresh patches.

 -- Boyuan Yang <byang@debian.org>  Thu, 18 Aug 2022 14:13:54 -0400

pdm-pep517 (1.0.3+ds1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches: Refresh patches.

 -- Boyuan Yang <byang@debian.org>  Sat, 30 Jul 2022 15:52:47 -0400

pdm-pep517 (1.0.2+ds1-2) unstable; urgency=high

  * debian/tests/autopkgtest-pkg-python.conf: Fix autopkgtest import name.

 -- Boyuan Yang <byang@debian.org>  Sun, 17 Jul 2022 10:46:05 -0400

pdm-pep517 (1.0.2+ds1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches: Refresh patches.
  * debian/control: Use autopkgtest-pkg-python testsuite.

 -- Boyuan Yang <byang@debian.org>  Tue, 12 Jul 2022 12:53:45 -0400

pdm-pep517 (1.0.1+ds-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches: Refresh patches.

 -- Boyuan Yang <byang@debian.org>  Sun, 03 Jul 2022 14:53:02 -0400

pdm-pep517 (1.0.0+ds-1) unstable; urgency=medium

  * Initial release. (Closes: #1014055)

 -- Boyuan Yang <byang@debian.org>  Wed, 29 Jun 2022 10:51:17 -0400
